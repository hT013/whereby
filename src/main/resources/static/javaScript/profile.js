let showMoreSettingFlag = false;
let deleteAccountSetting = document.getElementById("delete-account");
document.getElementById("show-delete-setting").addEventListener("click", function () {
    if(showMoreSettingFlag === false) {
        showMoreSettingFlag = true;
        deleteAccountSetting.style.display = "inline";
    }
    else if(showMoreSettingFlag === true) {
        showMoreSettingFlag =false;
        deleteAccountSetting.style.display = "none";
    }
})

function deleteAccount() {
    if(confirm("By pressing OK, your account will be deleted?")) {
        xhttp = new XMLHttpRequest();
        xhttp.open("DELETE","/user/delete",true);
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                console.log("deleted");
                alert("account deleted");
                window.location.href = "/auth/login";
            } else if (this.status === 400) {
                console.log("not deleted");
                alert("account could not be deleted");
            }
        }
        xhttp.send();
    }
    else {
        console.log("delete operation canceled");
    }
}

function updateDetails() {
    let name = document.getElementById("name").value;
    let email = document.getElementById("email").value;
    if(name === "") {
        document.getElementById("error-message").textContent = "name empty";
        return false;
    }
    else if(email === "") {
        document.getElementById("error-message").textContent = "email empty";
        return false;
    }
    return true;
}


document.getElementById('edit-btn').addEventListener('click',function (event) {
    event.preventDefault();
    document.getElementById('choose-file').click();
    document.getElementById('editdiv').style="display: none;";
    document.getElementById('savediv').style="display: block;";

})


function editImage(input,event) {
    event.preventDefault();
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image')
                .attr('src', e.target.result)
        };
        reader.readAsDataURL(input.files[0]);
        document.getElementById("choose-file").value = input;
    }
}

document.getElementById('savebtn').addEventListener('click',function (event) {
    event.preventDefault();
    saveImage();
});

function saveImage() {

    var form = $('#fileUploadForm')[0];
    var data = new FormData(form);
    var userId=document.getElementById('userId').value;
    $.ajax({
        type: "POST",
        enctype: 'multipart/form-data',
        url: "/user/save-image/"+userId,
        data: data,
        processData: false, //prevent jQuery from automatically transforming the data into a query string
        contentType: false,
        cache: false,
        success: (data) => {
            $("#listFiles").text(data);
        },
        error: (e) => {
            $("#listFiles").text(e.responseText);
        }
    });
    document.getElementById('savediv').style="display: none;";
    document.getElementById('editdiv').style="display: block;";
}
