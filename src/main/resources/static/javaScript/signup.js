
let checkTerms = document.getElementById("accept-terms");
let errorMessage = document.getElementById("error-message");
checkTerms.addEventListener('change', function () {
       let signUpButton = document.getElementById("sign-up-button");
       if(this.checked) {
           signUpButton.disabled = false;
       }
       else {
           signUpButton.disabled = true;
       }
})

window.addEventListener("load", function () {
     checkTerms.checked = false;
})


function validateRegistration () {
    console.log("from signup");
   if(document.getElementById("userName").value === "") {
       errorMessage.textContent="name field should not be empty";
       return false;
   }
   if(document.getElementById("userEmail").value==="") {
       errorMessage.textContent = "email field should not be empty"
       return false;
   }
   if(document.getElementById("userPassword").value === "") {
       errorMessage.textContent = "password should not be empty";
       return false;
   }
   if(document.getElementById("confirmPassword").value === "") {
       errorMessage.textContent = "confirm password should not be empty";
       return false;
   }

   if(document.getElementById("userPassword").value !==
       document.getElementById("confirmPassword").value) {
       errorMessage.textContent = "password mismatch";
       return false;
   }
   return true;
}

