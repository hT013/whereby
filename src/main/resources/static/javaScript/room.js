
let commentContainer = document.getElementById("comment-container");
let peopleContainer = document.getElementById("people-container");
let commentPeoplewindow = document.getElementById("comment-people-container");
let commentToggleButton = document.getElementById("comment-toggle-button");
let peopleToggleButton = document.getElementById("people-list-toggle-button");

let commentContainerFlag = false;
function showCommentWindow () {
    peopleContainer.style.display = "none";
    commentContainer.style.display = "block";
    commentToggleButton.style.backgroundColor = "blue";
    peopleToggleButton.style.backgroundColor = "rgba(0, 0, 0, 0.4)";

}
function showPeopleList () {
    commentContainer.style.display = "none";
    peopleContainer.style.display = "block";
    peopleToggleButton.style.backgroundColor = "blue";
    commentToggleButton.style.backgroundColor = "rgba(0, 0, 0, 0.4)";
}

function closeCommentPeopleWindow () {
    commentPeoplewindow.style.display = "none";
}

function showCommentPeopleBoxWithCommentWindow() {
    if(commentContainerFlag === false)  {
        commentContainerFlag = true;
        commentPeoplewindow.style.display = "block";
        showCommentWindow();
    }
    else if(commentContainer.style.display === "none"){
        showCommentWindow();
    }
    else {
        commentContainerFlag = false;
        closeCommentPeopleWindow();
    }
}

function showCommentPeopleBoxWithPeopleWindow() {
    if(commentContainerFlag === false)  {
        commentContainerFlag = true;
        commentPeoplewindow.style.display = "block";
        showPeopleList();
    }
    else if(peopleContainer.style.display === "none"){
        showPeopleList();
    }
    else {
        commentContainerFlag = false;
        closeCommentPeopleWindow();
    }
}

