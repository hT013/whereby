package com.mountblue.event;

import com.mountblue.entity.User;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class MailEvent extends ApplicationEvent {
    private static final long serialVersionUID = 1L;
    private String appUrl;
    private User user;
    private String subject;
    private String confirmationUrl;
    private String message;

    public MailEvent(User user, String appUrl, String subject, String confirmationUrl, String message) {
        super(user);
        this.user = user;
        this.appUrl = appUrl;
        this.subject = subject;
        this.confirmationUrl = confirmationUrl;
        this.message = message;
    }

}
