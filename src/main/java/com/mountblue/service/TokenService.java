package com.mountblue.service;

import com.mountblue.entity.Token;
import com.mountblue.entity.User;
import com.mountblue.repository.TokenRepository;
import org.springframework.stereotype.Service;

import static com.mountblue.constant.Error.*;

@Service
public class TokenService {

    private final TokenRepository tokenRepository;

    public TokenService(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    public void createToken(User user, String token) {
        Token newToken = new Token(token, user);
        tokenRepository.save(newToken);
    }

    public Token getToken(String tokenName) throws Exception {
        Token token;
        try {
            token = tokenRepository.findByToken(tokenName);
        } catch (Exception e) {
            throw new Exception(TOKEN_NOT_FOUND);
        }
        return token;
    }

    public void removeToken(Token token) {
        tokenRepository.delete(token);
    }

    public void deleteByUser(User user) {
        tokenRepository.deleteAllByUser(user);
    }
}
