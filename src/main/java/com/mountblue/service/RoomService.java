package com.mountblue.service;

import com.mountblue.entity.Room;
import com.mountblue.entity.User;
import com.mountblue.repository.RoomRepository;
import io.openvidu.java.client.*;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static com.mountblue.constant.Error.*;

@Service
public class RoomService {

    private final RoomRepository roomRepository;
    private final UserService userService;
    private final Map<String, Session> sessionMap;

    public RoomService(RoomRepository roomRepository, UserService userService) {
        this.roomRepository = roomRepository;
        this.userService = userService;
        sessionMap = new HashMap<>();
    }

    public boolean checkData(String roomName, Model model) {
        boolean result = true;
        if (roomName.length() == 0) {
            result = false;
            model.addAttribute(ERROR, NAME_CANNOT_EMPTY);
        }
        if (roomRepository.findByRoomName(roomName) != null) {
            result = false;
            model.addAttribute(ERROR, ROOM_ALREADY_EXIST);
        }
        return result;
    }

    public boolean checkRoomNotExist(String roomName, Model model) {
        if (roomRepository.findByRoomName(roomName) == null) {
            model.addAttribute(ERROR, ROOM_NOT_EXIST);
            return true;
        }
        return false;
    }

    public boolean checkAccess(String roomName, Model model, Authentication authentication) {
        if (userService.findUserByEmail(authentication.getName()).getId() != roomRepository.findByRoomName(roomName).getUserID()) {
            model.addAttribute(ERROR, ACCESS_DENIED);
            return false;
        }
        return true;
    }

    public String generateToken(Session session, HttpServletRequest request) {
        User user = userService.findUserByEmail(request.getRemoteUser());
        String encodedFile = null;
        if (user.getPicture() != null) {
            encodedFile = Base64.getEncoder().encodeToString(user.getPicture());
        }
        String serverData = "{" + "\"userName\":\"" + user.getName() + "\"" + "," +
                "\"userId\":\"" + user.getId() + "\"" + "," + "\"image\":\"" + encodedFile + "\"" + "}";
        String token = "";
        TokenOptions tokenOptions = new TokenOptions.Builder()
                .role(OpenViduRole.PUBLISHER)
                .data(serverData)
                .build();
        try {
            token = session.generateToken(tokenOptions);
        } catch (OpenViduJavaClientException | OpenViduHttpException e) {
            e.printStackTrace();
        }
        return token;
    }

    public void save(Session session, String roomName, String email) {
        User user = userService.findUserByEmail(email);
        Room room = new Room();
        room.setRoomName(roomName);
        room.setUserID(user.getId());
        sessionMap.put(roomName, session);
        roomRepository.save(room);
    }

    public Session getSession(String roomName) {
        return sessionMap.get(roomName);
    }

    public List<String> getRooms(Long id) {
        List<String> list = new ArrayList<>();
        roomRepository.findByUserID(id).forEach(room -> list.add(room.getRoomName()));
        return list;
    }

    public void delete(String roomName) {
        Room room = roomRepository.findByRoomName(roomName);
        roomRepository.delete(room);
        sessionMap.remove(roomName);
    }

}

