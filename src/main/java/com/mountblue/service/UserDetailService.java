package com.mountblue.service;

import com.mountblue.config.LocalUserDetails;
import com.mountblue.entity.User;
import com.mountblue.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.mountblue.constant.Error.*;

@Slf4j
@Service
@Transactional
public class UserDetailService implements UserDetailsService {

    private final UserRepository userRepository;

    public UserDetailService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByEmail(username);
        if (user == null) {
            log.info("user not found");
            throw new UsernameNotFoundException(USER_NOT_FOUND);
        }
        log.info("inside of user detailsService");
        return new LocalUserDetails(user);
    }


}
