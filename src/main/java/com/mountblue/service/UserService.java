package com.mountblue.service;

import com.mountblue.exception.UserRelatedException;
import com.mountblue.config.SecurityConfig;
import com.mountblue.entity.User;
import com.mountblue.listener.RegistrationEmailListener;
import com.mountblue.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static com.mountblue.constant.Error.*;
import static com.mountblue.constant.Constant.*;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.Objects;

@Slf4j
@Service
public class UserService {

    private final UserRepository userRepository;
    private final SecurityConfig securityConfig;
    private final TokenService tokenService;
    private final RegistrationEmailListener registrationEmailListener;

    public UserService(UserRepository userRepository, SecurityConfig securityConfig, TokenService tokenService,
                       RegistrationEmailListener registrationEmailListener) {
        this.userRepository = userRepository;
        this.securityConfig = securityConfig;
        this.tokenService = tokenService;
        this.registrationEmailListener = registrationEmailListener;
    }


    public User registerNewUserAccount(User user) throws UserRelatedException.UserExistException, UserRelatedException.PasswordMisMatchException {
        User registered = userRepository.findByEmail(user.getEmail());
        if (registered != null) {
            if (registered.isEnabled()) {
                log.error("Email taken");
                throw new UserRelatedException.UserExistException(USER_EXIST);
            } else if (!registered.isEnabled()) {
                registered.setName(user.getName());
                registered.setPassword(securityConfig.passwordEncoder().encode(user.getPassword()));
                userRepository.save(registered);
                return registered;
            }
        }

        if (!user.getPassword().equals(user.getConfirmPassword())) {
            log.error("Password mismatch");
            throw new UserRelatedException.PasswordMisMatchException(PASSWORD_SHOULD_MATCH);
        }
        user.setPassword(securityConfig.passwordEncoder().encode(user.getPassword()));
        user.setEnabled(false);
        user.setRole(USER);
        userRepository.save(user);
        return user;
    }

    public void saveUser(User user) {
        userRepository.save(user);
    }

    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }


    public void changePassword(User user, String password) {
        user.setPassword(securityConfig.passwordEncoder().encode(password));
        userRepository.save(user);
    }

    @Transactional
    public void deleteUser(User user) {
        try {
            tokenService.deleteByUser(user);
            userRepository.delete(user);

        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(USER_NOT_FOUND);
        }
    }

    public void updateUserName(User user, String name) {
        user.setName(name);
        userRepository.save(user);
    }

    public void updateUserEmail(User user, String email) {
        user.setEmail(email);
        userRepository.save(user);
    }

    public void sendResetEmail(User user, String email, String token, String appUrl) {
        tokenService.createToken(user, token);
        String confirmationUrl = LINK + token + EMAIL_ARG + email;
        String contentMessage = CHANGE_EMAIL_MESSAGE + appUrl + confirmationUrl;
        registrationEmailListener.send(email, contentMessage, CHANGE_EMAIL_SUBJECT);
    }

    public User saveImage(long id, byte[] bytes) {
        User user = userRepository.findById(id).get();
        user.setPicture(bytes);
        userRepository.save(user);
        return user;
    }

    public String getLocalImage() {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource("static/images/download.png")).getFile());
        String encodedFile = null;
        try {
            byte[] fileContent = FileUtils.readFileToByteArray(file);
            encodedFile = Base64.getEncoder().encodeToString(fileContent);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return encodedFile;

    }
}
