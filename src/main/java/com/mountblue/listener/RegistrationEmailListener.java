package com.mountblue.listener;

import com.mountblue.event.MailEvent;
import com.mountblue.service.TokenService;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.UUID;

import static com.mountblue.constant.Constant.*;

@Service
@Component
public class RegistrationEmailListener {

    public String globalToken = null;
    private final TokenService tokenService;
    private final SendGrid sendGrid;
    @Value("${sendgrid.mail}")
    private String email;

    public RegistrationEmailListener(TokenService tokenService, SendGrid sendGrid) {
        this.tokenService = tokenService;
        this.sendGrid = sendGrid;
    }


    @EventListener
    public void sendEmail(MailEvent mailEvent) {
        String token = UUID.randomUUID().toString();
        globalToken = token;
        tokenService.createToken(mailEvent.getUser(), token);
        String recipientAddress = mailEvent.getUser().getEmail();
        String subject = mailEvent.getSubject();
        String confirmationUrl = mailEvent.getConfirmationUrl() + token;
        String message = mailEvent.getMessage() + "\n";
        String contentMessage = message + mailEvent.getAppUrl() + confirmationUrl;
        this.send(recipientAddress, contentMessage, subject);
    }

    public void send(String recipientAddress, String contentMessage, String subject) {
        try {
            Email from = new Email(email);
            Email to = new Email(recipientAddress);
            Content content = new Content(TEXT_PLAIN, contentMessage);
            Mail mail = new Mail(from, subject, to, content);
            Request request = new Request();
            request.setMethod(Method.POST);
            request.setEndpoint(MAIL_SEND);
            request.setBody(mail.build());
            sendGrid.api(request);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getGlobalToken() {
        return globalToken;
    }

}


