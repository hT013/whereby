package com.mountblue.exception;

import javax.naming.AuthenticationException;

public class UserRelatedException {
    public static class UserExistException extends AuthenticationException {
        public UserExistException(final String msg) {
            super(msg);
        }
    }

    public static class PasswordMisMatchException extends AuthenticationException {
        public PasswordMisMatchException(final String msg) {
            super(msg);
        }
    }
}
