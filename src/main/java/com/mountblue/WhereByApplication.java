package com.mountblue;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class)
public class WhereByApplication {

    public static void main(String[] args) {
        SpringApplication.run(WhereByApplication.class, args);
    }

}
