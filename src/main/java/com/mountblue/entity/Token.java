package com.mountblue.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

@Getter
@Setter
@Entity(name = "tokens")
public class Token {

    private static final int EXPIRATION = 60 * 24;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String token;

    @OneToOne
    @JsonIgnore
    private User user;

    private Date expiryDate = calculateExpiryDate();

    private Date calculateExpiryDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        cal.add(Calendar.MINUTE, Token.EXPIRATION);
        return new Date(cal.getTime().getTime());
    }

    public Token() {
    }

    public Token(String token, User user) {
        this.token = token;
        this.user = user;
    }

}
