package com.mountblue.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @NotNull
    @NotEmpty
    String name;
    @NotNull
    @NotEmpty
    String email;
    @NotNull
    @NotEmpty
    String password;
    @Transient
    String confirmPassword;
    boolean enabled;
    boolean termsAccepted;
    String role;
    @OneToOne(cascade = CascadeType.ALL)
    @Transient
    private Token token;

    @Lob
    private byte[] picture;

}

