package com.mountblue.config;

import io.openvidu.java.client.OpenVidu;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenViduConfig {

    @Value("${openvidu.path}")
    private String path;
    @Value("${openvidu.secret}")
    private String secret;

    @Bean
    public OpenVidu getOpenVidu() {
        return new OpenVidu(path, secret);
    }

}
