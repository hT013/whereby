package com.mountblue.config;

import com.mountblue.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class LocalUserDetails implements UserDetails {
    private final String password;
    private final String email;
    private final boolean enabled;
    private final Set<GrantedAuthority> authorities = new HashSet<>();

    public LocalUserDetails(User user) {
        Logger logger = LoggerFactory.getLogger(LocalUserDetails.class);
        logger.info("inside userDetails");
        this.email = user.getEmail();
        this.password = user.getPassword();
        authorities.add(new SimpleGrantedAuthority(user.getRole()));
        this.enabled = user.isEnabled();
        if (email == null || password == null) {
            logger.error("null value");
        }
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

}
