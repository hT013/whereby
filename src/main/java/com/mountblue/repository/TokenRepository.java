package com.mountblue.repository;

import com.mountblue.entity.Token;
import com.mountblue.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends JpaRepository<Token, Long> {
    Token findByToken(String token) throws Exception;

    void deleteAllByUser(User user);
}
