package com.mountblue.repository;

import com.mountblue.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoomRepository extends JpaRepository<Room, Integer> {

    Room findByRoomName(String name);

    List<Room> findByUserID(Long userId);

}
