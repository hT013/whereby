package com.mountblue.constant;

public class Constant {

    public static final String EMPTY = "";
    public static final String IMAGE_TYPE = "png";
    public static final String IMAGE_PATH = "finaloutput.png";
    public static final String _USER_ = "user";
    public static final String EMAIL = "email";
    public static final String USERNAME = "username";
    public static final String IMAGE = "image";
    public static final String ROOMS = "rooms";
    public static final String TOKEN = "token";
    public static final String SESSION_NAME = "sessionName";
    public static final String NICK_NAME = "nickName";
    public static final String USER_NAME = "userName";
    public static final String USER_ID = "userId";
    public static final String USER = "USER";
    public static final String SUCCESSFULLY_DELETED = "successfully deleted";

    public static final String TEXT_PLAIN = "text/plain";
    public static final String MAIL_SEND = "mail/send";
    public static final String CHANGE_EMAIL_MESSAGE = "Click the below link to change email :\n";
    public static final String LINK = "/user/resetEmail?token=";
    public static final String CONFORMATION_URL = "/auth/registrationConfirm?token=";
    public static final String EMAIL_ARG = "&email=";
    public static final String RESET_PASSWORD_SUBJECT = "Reset Password";
    public static final String CHANGE_EMAIL_SUBJECT = "Change Email";
    public static final String SUBJECT = "User registration";
    public static final String CONFORM_MESSAGE = "Click the below link to complete registration";
    public static final String PASSWORD_RESET_URL = "/auth/getResetPasswordForm?token=";
    public static final String RESET_PASSWORD_MESSAGE = "Click the below link to change password";

}
