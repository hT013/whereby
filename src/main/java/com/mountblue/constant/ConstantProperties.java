package com.mountblue.constant;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@Setter
@Getter
@ConfigurationProperties
@PropertySource("classpath:constants.properties")
public class ConstantProperties {
    String userExist;
    String passwordMismatch;
    String errorOccurred;
    String tokenStringEmpty;
    String tokenNotFound;
    String tokenExpired;
    String accountVerified;
    String emailError;
    String emailSent;
    String emailAddressEmpty;
    String userNotFoundEmail;
    String passwordChangeSuccessful;
    String incorrectCredentials;
    String passwordEmpty;

}
