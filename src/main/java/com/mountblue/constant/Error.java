package com.mountblue.constant;

public class Error {

    public static final String ERROR = "error";
    public static final String NAME_CANNOT_EMPTY = "Name cannot be empty";
    public static final String ROOM_ALREADY_EXIST = "Room already exist";
    public static final String ROOM_NOT_EXIST = "Room doesn't exist";
    public static final String FIELDS_EMPTY = "Fields empty";
    public static final String TOKEN_NOT_FOUND = "Token Not Found";
    public static final String PASSWORD_SHOULD_MATCH = "Password Should Match";
    public static final String USER_EXIST = "User already exist!";
    public static final String EMAIL_EXIST = "Email already exist!";
    public static final String USER_NOT_FOUND = "User Not Found";
    public static final String ACCESS_DENIED = "Access Denied";

}
