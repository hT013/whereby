package com.mountblue.controller;

import com.mountblue.constant.ConstantProperties;
import com.mountblue.entity.Token;
import com.mountblue.entity.User;
import com.mountblue.service.TokenService;
import com.mountblue.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.Base64;
import java.util.UUID;

import static com.mountblue.constant.Constant.*;
import static com.mountblue.constant.Error.*;

@Slf4j
@Controller
@RequestMapping("/user/")
public class UserController {

    private final UserService userService;
    private final TokenService tokenService;
    private final ConstantProperties constantProperties;

    public UserController(UserService userService, TokenService tokenService, ConstantProperties constantProperties) {
        this.userService = userService;
        this.tokenService = tokenService;
        this.constantProperties = constantProperties;
    }

    @GetMapping("profile")
    public String profile(Model model, Authentication authentication, ModelMap map) {
        log.info("inside profile api");
        User user = userService.findUserByEmail(authentication.getName());
        model.addAttribute(ERROR, map.getAttribute(ERROR));
        model.addAttribute(USERNAME, user.getName());
        model.addAttribute(EMAIL, user.getEmail());
        model.addAttribute(USER_ID, user.getId());
        model.addAttribute(_USER_, user);
        if (user.getPicture() != null) {
            String encodedFile = Base64.getEncoder().encodeToString(user.getPicture());
            model.addAttribute(IMAGE, encodedFile);
        } else {
            String image = userService.getLocalImage();
            model.addAttribute(IMAGE, image);
        }
        return "profile";
    }

    @DeleteMapping("delete")
    @ResponseBody
    public ResponseEntity<?> deleteAccount(Authentication authentication) {
        log.info("inside delete account controller");
        User user = userService.findUserByEmail(authentication.getName());
        try {
            userService.deleteUser(user);
            authentication.setAuthenticated(false);
            return new ResponseEntity<>(SUCCESSFULLY_DELETED, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            log.error("error occurred");
            e.printStackTrace();
            return new ResponseEntity<>(ERROR, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("update")
    public String updateDetails(@RequestParam("username") String name, @RequestParam("email") String email,
                                Authentication authentication, RedirectAttributes redirectAttributes, HttpServletRequest request) {
        log.info(name + email);
        User user = userService.findUserByEmail(authentication.getName());
        if (name == null || email == null) {
            redirectAttributes.addFlashAttribute(ERROR, FIELDS_EMPTY);
            return "redirect:/user/profile";
        } else if (authentication.getName().equals(email)) {
            userService.updateUserName(user, name);
        } else if (userService.findUserByEmail(email) != null) {
            redirectAttributes.addFlashAttribute(ERROR, EMAIL_EXIST);
            return "redirect:/user/profile";
        } else {
            userService.updateUserName(user, name);
            String appUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
            String token = UUID.randomUUID().toString();
            userService.sendResetEmail(user, email, token, appUrl);
            return "email-send-message";
        }
        return "redirect:/user/profile";
    }

    @GetMapping("resetEmail")
    public String updateEmail(@RequestParam("token") String tokenString, @RequestParam("email") String email,
                              RedirectAttributes redirectAttributes, Authentication authentication) {
        if (email.equals(EMPTY)) {
            log.error("email empty");
            redirectAttributes.addFlashAttribute(ERROR, constantProperties.getEmailAddressEmpty());
            return "redirect:/user/profile";
        }
        if (tokenString.equals(EMPTY)) {
            log.error("token string empty");
            redirectAttributes.addFlashAttribute(ERROR, constantProperties.getTokenStringEmpty());
            return "redirect:/user/profile";
        }
        Token token;
        try {
            token = tokenService.getToken(tokenString);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("token not found");
            redirectAttributes.addFlashAttribute(ERROR, constantProperties.getTokenNotFound());
            return "redirect:/user/profile";
        }
        User user = token.getUser();
        authentication.setAuthenticated(false);
        userService.updateUserEmail(user, email);
        return "redirect:/auth/login";
    }


    @PostMapping("/save-image/{id}")
    public String editMultipartFile(@RequestParam("file") MultipartFile file, @PathVariable("id") long id) {
        try {
            User user = userService.saveImage(id, file.getBytes());
            ByteArrayInputStream bis2 = new ByteArrayInputStream(user.getPicture());
            BufferedImage bImage3 = ImageIO.read(bis2);
            ImageIO.write(bImage3, IMAGE_TYPE, new File(IMAGE_PATH));
            log.info("image edit");

        } catch (Exception e) {
            log.error("FAIL! Maybe You had uploaded the file before or the file's size > 500KB");
        }
        return "redirect:/user/profile";
    }
}

