package com.mountblue.controller;

import com.mountblue.entity.User;
import com.mountblue.service.RoomService;
import com.mountblue.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Base64;

import static com.mountblue.constant.Constant.*;

@Controller
public class HomeController {

    private final UserService userService;
    private final RoomService roomService;

    public HomeController(UserService userService, RoomService roomService) {
        this.userService = userService;
        this.roomService = roomService;
    }

    @GetMapping("/")
    public String homePage() {
        return "index";
    }

    @GetMapping("/home")
    public String home(Model model, Authentication authentication) {
        User user = userService.findUserByEmail(authentication.getName());
        model.addAttribute(USERNAME, user.getName());
        if (user.getPicture() != null) {
            String encodedFile = Base64.getEncoder().encodeToString(user.getPicture());
            model.addAttribute(IMAGE, encodedFile);
        } else {
            String image = userService.getLocalImage();
            model.addAttribute(IMAGE, image);
        }
        model.addAttribute(ROOMS, roomService.getRooms(user.getId()));
        return "home";
    }

    @GetMapping("room")
    public String room(Model model, HttpServletRequest request) {
        String email = request.getRemoteUser();
        User user = userService.findUserByEmail(email);
        if (user.getPicture() != null) {
            String encodedFile = Base64.getEncoder().encodeToString(user.getPicture());
            model.addAttribute(IMAGE, encodedFile);
        } else {
            String image = userService.getLocalImage();
            model.addAttribute(IMAGE, image);
        }
        return "room";
    }
}