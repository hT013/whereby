package com.mountblue.controller;

import com.mountblue.constant.ConstantProperties;
import com.mountblue.exception.UserRelatedException;
import com.mountblue.entity.Token;
import com.mountblue.event.MailEvent;
import com.mountblue.entity.User;
import com.mountblue.listener.RegistrationEmailListener;
import com.mountblue.service.TokenService;
import com.mountblue.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;

import static com.mountblue.constant.Constant.*;
import static com.mountblue.constant.Error.*;

@Slf4j
@Controller
@RequestMapping("/auth/")
public class AuthenticationController {

    private final UserService userService;
    private final TokenService tokenService;
    private final ApplicationEventPublisher eventPublisher;
    private final ConstantProperties constantProperties;
    private final RegistrationEmailListener registrationEmailListener;

    public AuthenticationController(UserService userService, TokenService tokenService, ApplicationEventPublisher eventPublisher, ConstantProperties constantProperties, RegistrationEmailListener registrationEmailListener) {
        this.userService = userService;
        this.tokenService = tokenService;
        this.eventPublisher = eventPublisher;
        this.constantProperties = constantProperties;
        this.registrationEmailListener = registrationEmailListener;
    }

    @GetMapping("sign-up")
    public String signUp(Model model) {
        model.addAttribute(_USER_, new User());
        return "sign-up";
    }

    @PostMapping("register")
    public String registerNewUser(@ModelAttribute("user") User user, HttpServletRequest request, Model model) {
        User registered;
        try {
            registered = createUserAccount(user);
        } catch (UserRelatedException.UserExistException e) {
            log.error("user exist");
            e.printStackTrace();
            model.addAttribute(ERROR, constantProperties.getUserExist());
            return "sign-up";
        } catch (UserRelatedException.PasswordMisMatchException e) {
            log.error("password mismatch");
            e.printStackTrace();
            model.addAttribute(ERROR, constantProperties.getPasswordMismatch());
            return "sign-up";
        }
        if (registered == null) {
            log.error("registration failed with registered user null");
            model.addAttribute(ERROR, constantProperties.getErrorOccurred());
            return "sign-up";
        }
        try {
            String appUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
            eventPublisher.publishEvent(new MailEvent(registered, appUrl, SUBJECT, CONFORMATION_URL, CONFORM_MESSAGE));
        } catch (Exception e) {
            log.error("error with event publish");
            model.addAttribute(ERROR, constantProperties.getErrorOccurred());
            return "sign-up";
        }
        String token = registrationEmailListener.getGlobalToken();
        model.addAttribute(TOKEN, token);
        return "resend-email";
    }

    private User createUserAccount(User user) throws UserRelatedException.UserExistException, UserRelatedException.PasswordMisMatchException {
        User registered;
        try {
            registered = userService.registerNewUserAccount(user);
        } catch (UserRelatedException.UserExistException e) {
            throw new UserRelatedException.UserExistException(USER_EXIST);
        } catch (UserRelatedException.PasswordMisMatchException e) {
            throw new UserRelatedException.PasswordMisMatchException(PASSWORD_SHOULD_MATCH);
        }
        return registered;
    }

    @GetMapping("/registrationConfirm")
    public String RegistrationConfirm(Model model, @RequestParam("token") String tokenName) {
        if (tokenName.equals(EMPTY)) {
            log.error("token Name empty");
            model.addAttribute(ERROR, constantProperties.getTokenStringEmpty());
            return "sign-up";
        }
        Token token;
        try {
            token = tokenService.getToken(tokenName);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("token not found");
            model.addAttribute(ERROR, constantProperties.getTokenNotFound());
            return "sign-up";
        }
        User user = token.getUser();

        Calendar cal = Calendar.getInstance();
        if ((token.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            log.error("token expired");
            model.addAttribute(ERROR, constantProperties.getTokenExpired());
            return "sign-up";
        }
        user.setEnabled(true);
        userService.saveUser(user);
        tokenService.removeToken(token);
        return "redirect:/auth/login";
    }

    @GetMapping("resendEmail/{token}")
    public String resendEmail(@PathVariable("token") String tokenString, HttpServletRequest request, Model model) {
        log.info(tokenString);
        if (tokenString.equals(EMPTY)) {
            log.error("token string empty");
            model.addAttribute(ERROR, constantProperties.getTokenStringEmpty());
            return "redirect:/auth/sign-up";
        }
        Token token;
        try {
            token = tokenService.getToken(tokenString);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("token not found");
            model.addAttribute(ERROR, constantProperties.getTokenNotFound());
            return "redirect:/auth/sign-up";
        }
        User user = token.getUser();
        String appUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        tokenService.removeToken(token);
        log.info(tokenString);
        eventPublisher.publishEvent(new MailEvent(user, appUrl, SUBJECT, CONFORMATION_URL, CONFORM_MESSAGE));
        return "resend-email";
    }

    @GetMapping("login")
    public String logIn(ModelMap map, Model model) {
        model.addAttribute(ERROR, map.getAttribute(ERROR));
        return "login";
    }

    @GetMapping("loginError")
    public RedirectView loginError(RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute(ERROR, constantProperties.getIncorrectCredentials());
        return new RedirectView("login");
    }

    @GetMapping("/forgotPassword")
    public String showPasswordResetForm() {
        return "forgot-password";
    }

    @PostMapping("/resetPasswordSendEmail")
    public String sendPasswordChangeEmail(HttpServletRequest request, @RequestParam("email") String email,
                                          Model model) {
        if (email.equals(EMPTY)) {
            log.error("username empty");
            model.addAttribute(ERROR, constantProperties.getEmailAddressEmpty());
            return "forgot-password";
        }
        User user;
        try {
            user = userService.findUserByEmail(email);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("user not found");
            model.addAttribute(ERROR, constantProperties.getUserNotFoundEmail());
            return "forgot-password";
        }
        if (user == null) {
            model.addAttribute(ERROR, constantProperties.getUserNotFoundEmail());
            return "forgot-password";
        }
        String appUrl = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort();
        eventPublisher.publishEvent(new MailEvent(user, appUrl, RESET_PASSWORD_SUBJECT, PASSWORD_RESET_URL, RESET_PASSWORD_MESSAGE));

        return "email-send-message";
    }

    @GetMapping("/getResetPasswordForm")
    public String showResetPasswordForm(Model model, RedirectAttributes redirectAttributes, @RequestParam("token") String resetToken) {
        log.info("inside get reset password form");
        if (resetToken.equals(EMPTY)) {
            log.error("token string empty");
            redirectAttributes.addFlashAttribute(ERROR, constantProperties.getTokenStringEmpty());
            return "redirect:/auth/login";
        }
        Token token;
        try {
            token = tokenService.getToken(resetToken);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("token not found");
            redirectAttributes.addFlashAttribute(ERROR, constantProperties.getTokenNotFound());
            return "redirect:/auth/login";
        }
        Calendar cal = Calendar.getInstance();
        if ((token.getExpiryDate().getTime() - cal.getTime().getTime()) <= 0) {
            log.error("token expired");
            redirectAttributes.addFlashAttribute(ERROR, constantProperties.getTokenExpired());
            return "redirect:/auth/login";
        }
        model.addAttribute(TOKEN, token.getToken());
        return "reset-password";
    }

    @PostMapping("/resetPassword/{token}")
    public String resetPassword(@RequestParam("password") String password, @RequestParam("confirmPassword")
            String confirmPassword, @PathVariable String token, Model model) {
        if (token.equals(EMPTY)) {
            log.error("token string empty");
            model.addAttribute(ERROR, constantProperties.getTokenStringEmpty());
            return "reset-password";
        }
        if (password == null || password.equals(EMPTY)) {
            log.error("password empty");
            model.addAttribute(ERROR, constantProperties.getPasswordEmpty());
            return "reset-password";
        }
        if (!password.equals(confirmPassword)) {
            log.error("password mismatch");
            model.addAttribute(ERROR, constantProperties.getPasswordMismatch());
            return "reset-password";
        }
        User user;
        try {
            user = tokenService.getToken(token).getUser();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("token not found");
            model.addAttribute(ERROR, constantProperties.getTokenNotFound());
            return "reset-password";
        }
        userService.changePassword(user, password);
        try {
            tokenService.removeToken(tokenService.getToken(token));
        } catch (Exception e) {
            e.printStackTrace();
            log.error("token not found");
            return "redirect:/auth/login";
        }
        return "redirect:/auth/login";
    }

}
