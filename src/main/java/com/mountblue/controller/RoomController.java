package com.mountblue.controller;

import com.mountblue.entity.User;
import com.mountblue.service.RoomService;
import com.mountblue.service.UserService;
import io.openvidu.java.client.*;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Base64;

import static com.mountblue.constant.Constant.*;

@Controller
@RequestMapping("/room")
public class RoomController {

    private final OpenVidu openVidu;
    private final UserService userService;
    private final RoomService roomService;

    public RoomController(OpenVidu openVidu, UserService userService, RoomService roomService) {
        this.openVidu = openVidu;
        this.userService = userService;
        this.roomService = roomService;
    }

    private String addRooms(String email, Model model) {
        User user = userService.findUserByEmail(email);
        model.addAttribute(ROOMS, roomService.getRooms(user.getId()));
        return "home";
    }

    private void setModelAttributes(Model model, String token, String sessionName, Authentication authentication) {
        User user = userService.findUserByEmail(authentication.getName());
        model.addAttribute(TOKEN, token);
        model.addAttribute(SESSION_NAME, sessionName);
        model.addAttribute(NICK_NAME, user.getName());
        model.addAttribute(USER_NAME, authentication.getName());
        model.addAttribute(USER_ID, user.getId());
        if (user.getPicture() != null) {
            String encodedFile = Base64.getEncoder().encodeToString(user.getPicture());
            model.addAttribute(IMAGE, encodedFile);
        }
    }

    @PostMapping("/create-room")
    public String createSession(@RequestParam String roomName, Model model, Authentication authentication,
                                HttpServletRequest request) throws OpenViduJavaClientException, OpenViduHttpException {
        if (!roomService.checkData(roomName, model)) {
            return addRooms(authentication.getName(), model);
        }
        SessionProperties properties = new SessionProperties.Builder().build();
        Session session = openVidu.createSession(properties);
        session.getSessionId();
        String token = roomService.generateToken(session, request);
        roomService.save(session, roomName, authentication.getName());
        setModelAttributes(model, token, roomName, authentication);
        return "room";
    }

    @RequestMapping("/goto-room")
    public String gotoRoom(@RequestParam String roomName, Model model, Authentication authentication,
                           HttpServletRequest request) throws OpenViduJavaClientException, OpenViduHttpException {
        if (roomService.checkRoomNotExist(roomName, model) || !roomService.checkAccess(roomName, model, authentication)) {
            return addRooms(authentication.getName(), model);
        }
        roomService.delete(roomName);
        createSession(roomName, model, authentication, request);
        return "room";
    }

    @RequestMapping("/join-room")
    public String joinSession(@RequestParam String roomName, Model model, Authentication authentication,
                              HttpServletRequest request) {
        if (roomService.checkRoomNotExist(roomName, model)) {
            return addRooms(authentication.getName(), model);
        }
        String token = roomService.generateToken(roomService.getSession(roomName), request);
        setModelAttributes(model, token, roomName, authentication);
        return "room";
    }

    @RequestMapping("/leave-room")
    public String leaveSession() {
        return "redirect:/home";
    }

    @RequestMapping("/delete")
    public String deleteRoom(@RequestParam String roomName, Model model, Authentication authentication) {
        if (!roomService.checkRoomNotExist(roomName, model) && roomService.checkAccess(roomName, model, authentication)) {
            roomService.delete(roomName);
            return "redirect:/home";
        }
        return addRooms(authentication.getName(), model);
    }

}
